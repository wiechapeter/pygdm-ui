Changelog
******************

[v0.2.1] - 2021-03-08
=====================

small bugfixes 


[v0.2.0] - 2021-02-04
=====================

- adapted to new pyGDM API

added
--------------
- added internal magnetic fields calculation and visualization / animation
- added optical forces


[v0.1.1] - 2020-03-28
=====================

several bugfixes


[v0.1] - 2020-01-15
=====================

first alpha version with full release functionality


[v0.0.1] - 2020-01-05
=====================

initial dummy version with no functionality
