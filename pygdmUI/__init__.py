# encoding: utf-8
#
#Copyright (C) 2020, P. R. Wiecha
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
    pygdmUI - a GUI for pyGDM2, a full-field electrodynamical solver python toolkit.
    
"""
__name__ = 'pygdmUI'
__version__ = '0.2.0'
__date__ = "03/03/2021"
__author__ = 'Peter R. Wiecha'

__all__ = ["main", "qt_ui_base", "custom_qt_objects", "pygdm_function_configs"]