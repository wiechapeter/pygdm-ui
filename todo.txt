- 2D simulations! --> project structure to y=0
- LDOS

- config: load and save defaults
- geo: list of meshpoint, remove meshpoints
- geo: load from text-file
- geo: 2D projections
- geo3D: plot axes or grid or something similar
- geo3D: color-legend for materials 
- material "fromfile" better path input


- GUI: docks (+ save last dock arrangement)



long-term:
- evolutionary optimization
- AI?
