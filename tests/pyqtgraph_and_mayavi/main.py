from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtGui import QPainter, QColor, QPen

import pyqtgraph as pg
import pyqtgraph.opengl as gl

import numpy as np

from pyGDM2 import structures

import sys

from qt_ui_base import *
from custom_qt_objects import Custom3DAxis



class MyApplication(QtGui.QMainWindow):
    def __init__(self, parent=None):
        print("THIS IS -- PYQT -- VERSION OF FEMTOPY!")
        super(MyApplication, self).__init__(parent)
        self.ui =  Ui_MainWindow()
        self.ui.setupUi(self)
        
        
        
# =============================================================================
#         opengl widget
# =============================================================================
        w = self.ui.widgetOpenGL
        w.setBackgroundColor(QColor(0, 0, 0))
        w.opts['distance'] = 30
        w.show()
        w.setWindowTitle('pyqtgraph example: GLScatterPlotItem')

        ## grid in substrate plane
        # gr = gl.GLGridItem()
        # w.addItem(gr)
        axis = Custom3DAxis(w, color=(0.9, 0.9, 0.9, 0.6))
        axis.setSize(x=12, y=12, z=12)
        axis.add_labels()
        axis.add_tick_values(xticks=[0,4,8,12], yticks=[0,6,12], zticks=[0,3,6,9,12])
        w.addItem(axis)
        
        
        ## scatter object
        # pos = np.empty((53, 3))
        # size = np.empty((53))
        # color = np.empty((53, 4))
        
        step = 1
        geo = structures.split_ring(step, R=10, W=3, H=3, mesh='hex')
        
        size = np.empty((len(geo)))
        color = np.empty((len(geo), 4))
        for i, p in enumerate(geo):
            size[i] = step / 1
            color[i] = (.3, 0.3, 0.7, 0.99)  # individual color
            
        sp1 = gl.GLScatterPlotItem(pos=geo, size=size, color=color, pxMode=False)
        w.addItem(sp1)
        
        
# =============================================================================
#         mayavi widget
# =============================================================================
        self.ui.mayaviWidgetGeo.visualization.plot_struct(geo)
        
        
    




# def main():
#     app = QtWidgets.QApplication(sys.argv)
#     main = Ui_MainWindow()
#     main.show()
#     sys.exit(app.exec_())

# if __name__ == '__main__':         
#     main()
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApplication()
    window.show()
    sys.exit(app.exec_()) 