# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt_ui_base.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1599, 914)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(800, 800))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.layoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(0, 0, 1507, 831))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.widgetOpenGL = GLViewWidget(self.layoutWidget)
        self.widgetOpenGL.setMinimumSize(QtCore.QSize(600, 800))
        self.widgetOpenGL.setObjectName("widgetOpenGL")
        self.gridLayout.addWidget(self.widgetOpenGL, 0, 1, 2, 1)
        self.lineEditFilename = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEditFilename.setObjectName("lineEditFilename")
        self.gridLayout.addWidget(self.lineEditFilename, 0, 0, 1, 1)
        self.pushButtonLoad = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButtonLoad.setObjectName("pushButtonLoad")
        self.gridLayout.addWidget(self.pushButtonLoad, 1, 0, 1, 1)
        self.mayaviWidgetGeo = MayaviQWidget(self.layoutWidget)
        self.mayaviWidgetGeo.setMinimumSize(QtCore.QSize(600, 800))
        self.mayaviWidgetGeo.setObjectName("mayaviWidgetGeo")
        self.gridLayout.addWidget(self.mayaviWidgetGeo, 0, 2, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1599, 20))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButtonLoad.setText(_translate("MainWindow", "load structure"))
from custom_qt_objects import MayaviQWidget
from pyqtgraph.opengl import GLViewWidget
